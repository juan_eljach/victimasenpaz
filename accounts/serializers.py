from django.contrib.auth.models import User
from rest_framework import serializers
from .models import UserProfile

class ProfileSerializerForUser(serializers.ModelSerializer):
	class Meta:
		model = UserProfile
		fields = (
			'user_type',
			'slug',
		)

class UserSerializer(serializers.ModelSerializer):
	profile = ProfileSerializerForUser()
	class Meta:
		model = User
		fields = (
			'id',
			'first_name',
			'last_name',
			'email',
			'profile',
		)

class UserProfileSerializer(serializers.ModelSerializer):
	user = UserSerializer()
	class Meta:
		model = UserProfile
		fields = (
			'id',
			'user',
			'user_type',
			'slug'
	)
