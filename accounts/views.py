from django.shortcuts import render
from rest_framework import generics
from .serializers import UserProfileSerializer
from .models import UserProfile

class PsychologistListAPIView(generics.ListAPIView):
	serializer_class = UserProfileSerializer
	model_class = UserProfile

	def get_queryset(self):
		return self.model_class.objects.filter(user_type="psicologo")

class PsychologistDetailAPIView(generics.RetrieveAPIView):
	lookup_field = "slug"
	serializer_class = UserProfileSerializer