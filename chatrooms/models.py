from django.db import models
from django.conf import settings

class ChatRoom(models.Model):
	users = models.ManyToManyField(settings.AUTH_PROFILE_MODULE)
	timestamp = models.DateTimeField(auto_now_add=True)
	is_over = models.BooleanField(default=False)

class Appointment(models.Model):
	date = models.DateTimeField()
	psychologist = models.ForeignKey(settings.AUTH_PROFILE_MODULE, related_name="psychologist")
	patient = models.ForeignKey(settings.AUTH_PROFILE_MODULE, related_name="patient")
	timestamp = models.DateTimeField(auto_now_add=True)