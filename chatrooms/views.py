from django.shortcuts import render
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from .models import Appointment
from .serializers import AppointmentCreateSerializer

class AppointmentCreateAPIView(generics.CreateAPIView):
	queryset = Appointment.objects.all()
	serializer_class = AppointmentCreateSerializer
	permission_classes = (IsAuthenticated,)