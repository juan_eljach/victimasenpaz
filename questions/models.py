from django.db import models
from django.template import defaultfilters
from hashlib import sha1
from django.core.urlresolvers import reverse
from accounts.models import UserProfile

class Question(models.Model):
	city = models.CharField(max_length=100)
	title = models.CharField(max_length=200)
	body = models.TextField(max_length=5000)
	user = models.ForeignKey(UserProfile, null=True, blank=True)
	slug = models.CharField(max_length=500)
	timestamp = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.title

	def save(self, *args, **kwargs):
		if not self.pk:
			self.slug = defaultfilters.slugify(self.title)
			while True:
				try:
					question_exists = Question.objects.get(slug__iexact=self.slug)
					if question_exists:
						self.slug = "{0}-{1}".format(self.slug, sha_constructor(str(random.random()).encode('utf-8')).hexdigest()[:5])
				except Question.DoesNotExist: break
		super(Question, self).save(*args, **kwargs)

#	def get_absolute_url(self):
#		return reverse('project_detail', kwargs={'project_slug': self.slug})

class Answer(models.Model):
	question = models.ForeignKey(Question, related_name="answers")
	psychologist = models.ForeignKey(UserProfile)
	body = models.TextField(max_length=10000)

	def __str__(self):
		return "{0}...".format(self.body[:20])
