from rest_framework import serializers
from .models import Question, Answer
from accounts.serializers import UserProfileSerializer

class AnswerCreateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Answer
		fields = (
			'question',
			'body',
		)

class AnswerSerializer(serializers.ModelSerializer):
	psychologist = UserProfileSerializer()
	class Meta:
		model = Answer
		fields = (
			'psychologist',
			'body',
		)

class QuestionSmallSerializer(serializers.ModelSerializer):
	number_of_answers = serializers.SerializerMethodField()
	class Meta:
		model = Question
		fields = (
			'title',
			'number_of_answers',
		)

	def get_number_of_answers(self, obj):
		return len(obj.answers.all())

class QuestionSerializer(serializers.ModelSerializer):
	answers = AnswerSerializer(many=True, read_only=True)
	class Meta:
		model = Question
		fields = (
			'id',
			'city',
			'title', 
			'body', 
			'slug',
			'timestamp',
			'answers',
        )

class QuestionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = (
			'city', 
			'title', 
			'body', 
			'timestamp'
	)