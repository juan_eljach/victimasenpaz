from django.shortcuts import render
from rest_framework import generics
from .serializers import QuestionSmallSerializer, QuestionSerializer, QuestionCreateSerializer, AnswerCreateSerializer
from .models import Question, Answer
from rest_framework.permissions import IsAuthenticated
from .permissions import IsOwnerOrForbidden

class QuestionListAPIView(generics.ListAPIView):
	serializer_class = QuestionSerializer
	model_class = Question
	queryset = Question.objects.all()

class QuestionListFilteredAPIView(generics.ListAPIView):
	serializer_class = QuestionSmallSerializer
	model_class = Question
	permission_classes = (IsAuthenticated,)

	def get_queryset(self):
		return self.model_class.objects.filter(user=self.request.user.profile)

class QuestionDetailAPIView(generics.RetrieveAPIView):
	serializer_class = QuestionSerializer
	model_class = Question
	queryset = Question.objects.all()
	lookup_field = "slug"
	lookup_url_kwarg = "question_slug"

class QuestionCreateAPIView(generics.CreateAPIView):
	queryset = Question.objects.all()
	serializer_class = QuestionCreateSerializer
	permission_classes = (IsAuthenticated,)

	def perform_create(self, serializer):
		serializer.save(user=self.request.user.profile)

class AnswerCreateAPIView(generics.CreateAPIView):
	queryset = Answer.objects.all()
	serializer_class = AnswerCreateSerializer
	permission_classes = (IsAuthenticated, IsOwnerOrForbidden,)

	def perform_create(self, serializer):
		print ("GOT INTO perform_create")
		serializer.save(psychologist=self.request.user.profile)