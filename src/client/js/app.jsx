// Only for development
module.hot.accept()
require('scss')

//dependencies
import React, { Component } from 'react'
import { render } from 'react-dom'
import $ from 'jquery'
import { Router, Route, hashHistory, IndexRoute } from 'react-router'

//components
import LogIn from './components/_login'
import Questions from './components/questions/_questions'
import MyQuestions from './components/questions/_myquestions'
import Psychologists from './components/psychologists/_psychologists'
import VideoCall from './components/videocall/_videocall'
import HeaderMobile from './components/header/_headermobile'
import Header from './components/header/_header'
import NavMobile from './components/header/_navMobile'


class App extends Component {

  constructor(props){
    super(props)
  }

  componentDidMount(){

  }

  render () {
    return (
      <div className="Main">

        <HeaderMobile />
        <Header />

        { this.props.children }

        <NavMobile/>

      </div>
    )
  }
}
render( (
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Questions}/>
      <Route path="/questions" component={ Questions }/>
      <Route path="/psychologists" component={ Psychologists }/>
      <Route path="/videocall" component={ VideoCall }/>
      <Route path="/myquestions" component={ MyQuestions }/>
    </Route>
  </Router>
), document.getElementById('App'))
