import React, {Component} from 'react'

export default class LogIn extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return (
      <section className="LogIn">
        <div className="LogIn__form">
          <div className="LogIn__formHeader">
            <div className="LogIn__formImg">
              <img src="/dist/images/logo.png" alt=""/>
            </div>
            <div className="LogIn__formRegister">
              <div>¿No tienes cuenta?</div>
              <div><a href="">Regístrate</a></div>
            </div>
          </div>
          <div className="LogIn__formBody">
            <div className="LogIn__formGroup">
              <label htmlFor="">Correo</label>
              <input type="text"/>
            </div>
            <div className="LogIn__formGroup">
              <label htmlFor="">Contraseña</label>
              <input type="text"/>
            </div>
            <div className="LogIn__formButton">
              <button>Iniciar sesión</button>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
