import React, {Component} from 'react'
import { Link } from 'react-router'

export default class Header extends Component{
  constructor(props){
    super(props)

    this.state = { itemSelected: 1 }
  }

  componentDidMount(){

  }

  selectNav(item){
    this.setState({ itemSelected: item })
  }

  render(){
    return (
      <header className="Header">
        <div className="Header__logo">
          <img src="/dist/images/logo.png" alt="" width="160"/>
        </div>
        <div className="Header__search">
          <input type="text" placeholder="Buscar alguna pregunta o persona"/>
        </div>
        <nav className="Header__nav">
          <ul>
            <li><Link to="/questions" onClick={()=> this.selectNav(1) } className={ this.state.itemSelected === 1 ? 'u-itemMenuActive' : ''}>Preguntas</Link></li>
            <li><Link to="/psychologists" onClick={()=> this.selectNav(2) } className={ this.state.itemSelected === 2 ? 'u-itemMenuActive' : '' }>Psicologos</Link></li>
            <li><a href="/#/videocall" onClick={()=> this.selectNav(3) } className={ this.state.itemSelected === 3 ? 'u-itemMenuActive' : '' }>Videollamada</a></li>
          </ul>
        </nav>
        <div className="Header__userInfo">
          <span>Juan Eljach</span>
        </div>
      </header>
    )
  }
}
