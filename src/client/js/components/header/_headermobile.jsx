import React, {Component} from 'react'

export default class HeaderMobile extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return (
      <header className="HeaderMobile">
        <div id="menuMobile">Menu</div>
        <div className="HeaderMobile__logo">
          <img src="/dist/images/logo.png" width="140" alt="logo-victimas-en-paz-mobile"/>
        </div>
        <div>Buscar</div>
      </header>
    )
  }
}
