import React, {Component} from 'react'
import { Link } from 'react-router'

export default class NavMobile extends Component{
  constructor(props){
    super(props)

    this.state = { itemSelected: 1 }
  }

  componentDidMount(){
    this.$menuMobile = $('#menuMobile')
    this.$navMobile = $('#navMobile')
    this.$navList = $('.NavMobile__body ul li')

    this.$menuMobile.click(()=> this.closeMenu())

    this.$navList.click(()=> this.closeMenu())

  }

  closeMenu(){
    this.$navMobile.toggleClass('u-left0')
  }

  selectNav(item){
    this.setState({ itemSelected: item })
  }

  render(){
    return (
      <nav id="navMobile" className="NavMobile">
        <div className="NavMobile__header">
          <img src="https://68.media.tumblr.com/avatar_9ff9f5a36638_128.png" width="50" alt=""/>
          <div>Juan Eljach</div>
        </div>
        <div className="NavMobile__body">
          <ul>
            <li><Link to="/questions" onClick={()=> this.selectNav(1) } className={ this.state.itemSelected === 1 ? 'u-itemMenuActive' : ''}>Preguntas</Link></li>
            <li><Link to="/psychologists" onClick={()=> this.selectNav(2) } className={ this.state.itemSelected === 2 ? 'u-itemMenuActive' : ''}>Psicologos</Link></li>
            <li><Link to="/videocall" onClick={()=> this.selectNav(3) } className={ this.state.itemSelected === 3 ? 'u-itemMenuActive' : ''}>Videollamada</Link></li>
            <li id="myQuestions"><Link to="/myquestions" onClick={()=> this.selectNav(4) } className={ this.state.itemSelected === 4 ? 'u-itemMenuActive' : ''}>Mis preguntas</Link></li>
          </ul>
        </div>
      </nav>
    )
  }
}
