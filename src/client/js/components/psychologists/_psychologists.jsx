import React, {Component} from 'react'

export default class Psychologists extends Component{
  constructor(props){
    super(props)

    this.state = {
      user: {},
      psychologists: []
    }

  }

  // date time field fecha y hora, id psicologo, id usuario --> rest-auth/user,

  componentDidMount(){

    $.ajax({
      url: 'http://127.0.0.1:8000/api/psicologos/',
      method: 'GET'
    })
    .done((data)=> {
      console.log(data);
      this.setState({ psychologists: data })
    })
    .fail((error)=> console.log(error))

    $.ajax({
      url: 'http://127.0.0.1:8000/rest-auth/user/',
      method: 'GET',
      headers: {
        'Authorization': `Token ${ window.localStorage.getItem('key') }`
      }
    })
    .done((data)=> {
      console.log(data)
      this.setState({user: data})
    })
    .fail((error)=> console.log(error))

    setTimeout(()=> {
      $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true});
      $('#psychologistsTimepicker').timepicker({ 'scrollDefault': 'now' });
    }, 200)

  }

  openAppointment = ()=> {
    $('#psychologistSendMsg').css({display: 'none'})
    $('#psychologistSelectDate').css({display: 'flex'})
  }

  openSendMsg = () => {
    $('#psychologistSelectDate').css({display: 'none'})
    $('#psychologistSendMsg').css({display: 'block'})
  }

  sendAppointment = (psychologistsId) => {

    // $.ajax({
    //   url: 'http://127.0.0.1:8000/appointment/create/',
    //   method: 'POST',
    //   data: {userId: '', psychologistId: '', date_time_field: ''}
    // })

    console.log($('#psychologistsDateInput').val())

  }

  render(){
    return (
      <section className="Psychologists">

        {
          this.state.psychologists.map((p, i)=>{
            return <div className="Psychologists__card" key={i}>
              <div className="Psychologists__info">
                <img src="https://68.media.tumblr.com/avatar_9ff9f5a36638_128.png" width="90" alt=""/>
                <h2>{ `${p.user.first_name} ${p.user.last_name}` }</h2>
                <p>Psicologa especialista en víctimas con 5 años de experiencia</p>
              </div>

              <div className="Psychologists__action">
                <button onClick={this.openSendMsg}>Enviar mensaje</button>
                <button onClick={this.openAppointment}>Solicitar cita</button>
              </div>

              <div className="Psychologists__sendMsg" id="psychologistSendMsg">
                <textarea name="" id="" placeholder="Escribe tu mensaje"></textarea>
                <button>Enviar</button>
              </div>

              <div className="Psychologists__selectDate" id="psychologistSelectDate">
                <div>
                  <label htmlFor="">Selecciona el día:</label>
                  <input id="psychologistsDateInput" data-toggle="datepicker"/>
                </div>
                <div>
                  <label htmlFor="">Selecciona la hora:</label>
                  <input id="psychologistsTimepicker"/>
                </div>
                <button onClick={()=> this.sendAppointment(p.id)}>Enviar</button>
              </div>

              <div className="Psychologists__msgResponse">
                <div>Tu mensaje ha sido enviado</div>
                <div>
                  <button>nuevo mensaje</button>
                  <button>cerrar</button>
                </div>
              </div>

              <div className="Psychologists__ciResponse">
                <div>Tu cita ha sido agendada para el 5 de octubre 2017 a las 3pm</div>
                <div>
                  <button>nueva cita</button>
                  <button>cerrar</button>
                </div>
              </div>
            </div>
          })

        }

      </section>
    )
  }
}
