import React, { Component } from 'react'
import MyQuestions from './_myquestions'

export default class Home extends Component{
  constructor(props){
    super(props)
    this.state = {questionsContent: []}
  }

  componentDidMount(){
    this.$titleInput = $('#titleInput')
    this.$bodyInput = $('#bodyInput')
    this.$cityInput = $('#cityInput')
    this.$sendButton = $('#sendButton')

    $.ajax({
      url: 'http://127.0.0.1:8000/api/questions/',
      method: 'GET'
    })
    .done((dataQ)=> {
      this.setState({questionsContent: dataQ})
    })
    .fail((error)=> console.log(error))
  }

  sendQuestion(){

    let inputsValid = this.$titleInput.val() && this.$cityInput.val() && this.$bodyInput.val()
    console.log(inputsValid);
    if(inputsValid){

      let DATA = { city: this.$cityInput.val(), title: this.$titleInput.val(), body: this.$bodyInput.val(), user: 3}
      console.log(DATA)
      $.ajax({
        url: 'http://127.0.0.1:8000/api/questions/create/',
        method: 'POST',
        data: DATA,
        headers: {
          'Authorization': `Token ${window.localStorage.getItem('key')}`
        }
      })
      .done((data)=> {
        console.log(data)
        this.$bodyInput.val('')
        this.$titleInput.val('')
        this.$cityInput.val('')

        this.props.updateMyQuestions(data)

        console.log(data);

      })
      .fail((error)=> console.log(error))
    }

  }


  render(){
    return (

      <div className="Content">

        <div className="Content__write">
          <div className="Content__writeHead">
            <div>
              <label htmlFor="">Título:</label>
              <input type="text" id="titleInput" placeholder="Escribe tu pregunta"/>
            </div>
            <div>
              <label htmlFor="">Ciudad:</label>
              <input type="text" id="cityInput" placeholder="¿Dónde te encuentras?"/>
            </div>
          </div>
          <textarea name="" id="bodyInput" cols="30" rows="10" placeholder="Agrega información adicional a tu pregunta"></textarea>
          <button id="sendButton" onClick={this.sendQuestion.bind(this)}>Enviar pregunta</button>
        </div>

        <h1>Preguntas populares:</h1>
        <div className="Content__wrapper">

          {
            this.state.questionsContent.map((q, i)=>{
              return <article key={i}>
                <h2>{ q.title }</h2>
                <span> { q.body } </span>

                {
                  (q ? q.answers : []).map((a, i)=>{
                    return <div className="Content__answer" key={i}>
                      <p><span>Respuesta: </span>{ a.body }</p>
                      <div className="Content__responseBy">
                        <div>Respondido por:</div>
                        <div>
                          <img src="https://68.media.tumblr.com/avatar_9ff9f5a36638_128.png" width="45" alt=""/>
                          <span> { `${a.psychologist.user.first_name} ${a.psychologist.user.last_name}` } </span>
                          <span>{ a.psychologist.user_type }</span>
                        </div>
                      </div>
                    </div>
                  })
                }

              </article>
            })
          }

        </div>

      </div>
    )
  }
}
