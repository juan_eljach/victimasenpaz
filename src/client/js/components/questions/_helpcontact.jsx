import React, {Component} from 'react'

export default class HelpContact extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return (
      <div className="HelpContact">
        <div className="HelpContact__header">
          ¿A quién pedir ayuda?
        </div>
        <div className="HelpContact__item">
          <span>Policia nacional</span>
          <span>marque 123</span>
        </div>
        <div className="HelpContact__item">
          <span>Hospital</span>
          <span>marque 245</span>
        </div>
        <div className="HelpContact__item">
          <span>ETI</span>
          <span>marque 245</span>
        </div>
        <div className="HelpContact__item">
          <span>Denuncias</span>
          <span>marque 245</span>
        </div>
      </div>
    )
  }
}
