import React, {Component} from 'react'

export default class MyQuestions extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return (
      <div className={ `MyQuestions ${ this.props.noDisplaymobile && $(window).width() < 1000 ? 'u-displayNone' : '' }` }>
        <div className="MyQuestions__wrapper">
          <div className="MyQuestions__header">
            <div>Mis preguntas</div>
          </div>
          <div className="MyQuestions__body">

            {
              this.props.myQuestions.map((q, i)=>{
                return <div className="MyQuestions__item" key={i}>
                  <div>{q.title}</div>
                  <div className="MyQuestions__answer">
                    <span> 1 respuesta</span>
                    <span>ver</span>
                  </div>
                </div>
              })
            }

          </div>
        </div>
      </div>
    )
  }
}
