import React, {Component} from 'react'
import Content from './_content'
import HelpContact from './_helpcontact'
import MyQuestions from './_myquestions'


// api/questions/create

export default class Questions extends Component{

  constructor(props){
    super(props)

    this.state = {
      myQuestions: []
    }
  }

  // /rest-auth/login

  componentDidMount(){

    $.ajax({
      url: 'http://127.0.0.1:8000/rest-auth/login/',
      method: 'POST',
      data: {
        email: 'davidjaimest@gmail.com',
        password: 'david123'
      }
    })
    .done((data) => {

       this.localStorageSaveKey(data.key)

       $.ajax({
         url: 'http://127.0.0.1:8000/api/myquestions/',
         method: 'GET',
         headers: {
           'Authorization': `Token ${data.key}`
         }
       })
       .done((dataP)=> {
         this.setState({myQuestions: dataP})
       })
       .fail((error)=> console.log(error))
     })
    .fail((error)=> console.log(error))
  }

  localStorageSaveKey(key){
    window.localStorage.setItem('key', key)
  }

  updateMyQuestions(data){
    this.setState({myQuestions: this.state.myQuestions.concat([data])})
  }

  render(){
    return (
      <section className="Questions">
        <HelpContact/>
        <Content updateMyQuestions={this.updateMyQuestions.bind(this)}/>
        <MyQuestions myQuestions={this.state.myQuestions} noDisplaymobile={true} />
      </section>
    )
  }
}
