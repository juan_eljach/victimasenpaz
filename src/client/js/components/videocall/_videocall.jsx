import React, {Component} from 'react'

export default class VideoCall extends Component{
  constructor(props){
    super(props)
    this.state = {
     chatMessages : [],
     user: { profile: '' }
    }
  }

  componentWillMount(){
    $.ajax({
      url: 'http://127.0.0.1:8000/rest-auth/user/',
      method: 'GET',
      headers: {
        'Authorization': `Token ${ window.localStorage.getItem('key') }`
      }
    })
    .done((dataU)=> {
      console.log(dataU)
      this.setState({user: dataU})
    })
    .fail((error)=> console.log(error))
  }

  componentDidMount(){

    this.$inputMessage = $('#inputMessage')
    this.$videoCallChatMessages = $('#videoCallChatMessages')
    this.socket = io.connect()
    this.socket.on('new message', (m)=> {
      this.setState({chatMessages: this.state.chatMessages.concat([m])})
    })

    $(document).on('keyup', (e)=>{
      if(this.$inputMessage.val()) console.log('asas');

    })

  }

  newMessage(i){
    this.socket.emit('message', this.$inputMessage.val())
    this.setState({chatMessages: this.state.chatMessages.concat([this.$inputMessage.val()])})
    this.$videoCallChatMessages.scrollTop(this.$videoCallChatMessages[0].scrollHeight)
    $('.videoCall__chat').scrollTop($('.videoCall__chat').height())
    this.$inputMessage.val('')
  }

  render(){
    return (
      <section className="VideoCall" ref="videoCall">
        <div className="VideoCall__video">

          {/* <div className="VideoCall__waiting">
            <img src="/dist/images/logo.png" alt=""/>
            <p>Un momento por favor...</p>
          </div> */}

          <div className="VideoCall__finished">
            <p>ha terminado tu videollamada</p>
            <span>Deseas:</span>
            <div className="VideoCall__actions">
              <div>Volver a llamar</div>
              <div>programar otra cita</div>
              <div>cerrar</div>
            </div>
          </div>

          <div className="VideoCall__controls">
            <span></span>
          </div>
        </div>

        <div className="VideoCall__chat">
          <div className="VideoCall__chatHeader">
            <img src="https://68.media.tumblr.com/avatar_9ff9f5a36638_128.png" alt=""/>
            <div><span> { this.state.user.profile.user_type === 'paciente' ? 'Usuario:' : 'Psicológo(a):'} </span> {`${this.state.user.first_name} ${this.state.user.last_name}` }</div>
          </div>
          <span>Están ahora en una video llamada</span>
          <div className="VideoCall__chatMessages" id="videoCallChatMessages">

            {
              this.state.chatMessages.map((m, k)=>{
                return <div className="VideoCall__chatMsgPsycho" key={k}>
                        <img src="https://68.media.tumblr.com/avatar_9ff9f5a36638_128.png" alt=""/>
                        <span>{m}</span>
                      </div>
                      {/* <div className="VideoCall__chatMsgUser">
                        <span>{m}</span>
                      </div> */}
              })
            }

          </div>

          <div className="VideoCall__chatWrite">
            <textarea id="inputMessage" placeholder="Escribe tu mensaje"/>
            <button onClick={this.newMessage.bind(this)}>Enviar</button>
          </div>
        </div>

      </section>
    )
  }
}
