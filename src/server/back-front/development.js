const express = require('express')
const path = require('path')
const webpack = require('webpack')
const config = require('../../../webpack.config.js')
const Dashboard = require('webpack-dashboard')
const DashboardPlugin = require('webpack-dashboard/plugin')
const app = express()
const compiler = webpack(config)
const dashboard = new Dashboard()

const server = require('http').Server(app)
const io = require('socket.io')(server)

io.on('connection', (socket) => {
  socket.on('message', (m) => {
    socket.broadcast.emit('new message', m)
  })
})

// Only for development
compiler.apply(new DashboardPlugin(dashboard.setData))
app.use(require('webpack-dev-middleware')(compiler, {
  quiet: true,
  publicPath: config.output.publicPath
}))
app.use(require('webpack-hot-middleware')(compiler, {
  log: () => {}
}))

app.use('/dist', express.static(path.resolve('./dist')))

const html = `<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Víctimas en paz Dev</title>
  </head>
  <body>
    <div id="App"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="http://192.168.1.67:8080/socket.io/socket.io.js"></script>
    <script src='dist/app.js'></script>
  </body>
  </html>`

app.get('/', (req, res)=>{
  res.send(html)
})

server.listen(8080, () => console.log('localhost:8080'))
