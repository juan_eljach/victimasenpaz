#CORE API
from django.conf.urls import url
from accounts.views import PsychologistListAPIView
from questions.views import QuestionListAPIView, QuestionListFilteredAPIView, QuestionDetailAPIView, QuestionCreateAPIView, AnswerCreateAPIView
from chatrooms.views import AppointmentCreateAPIView

urlpatterns = [
	#URL API configuration for Questions
	url(r'^questions/$', QuestionListAPIView.as_view(), name='questions_list'),
	url(r'^questions/detail/(?P<question_slug>[-\w]+)/$', QuestionDetailAPIView.as_view(), name='question_detail'),
	url(r'^myquestions/$', QuestionListFilteredAPIView.as_view(), name='myquestions_list'),
	url(r'^questions/create/$', QuestionCreateAPIView.as_view(), name='create_question'),
	url(r'^answers/create/$', AnswerCreateAPIView.as_view(), name='create_answer'),

	#URL API configuration for Accounts
	url(r'^psicologos/$', PsychologistListAPIView.as_view(), name='psychologist_list'),
	url(r'^psicologos/(?P<slug>[-\w]+)/$', PsychologistListAPIView.as_view(), name='psychologist_list'),

	url(r'^appointment/create/$', AppointmentCreateAPIView.as_view(), name='appointment_create'),

]