from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'victimasenpaz.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include("victimasenpaz.api", namespace="api")),
    url(r'^rest-auth/', include('rest_auth.urls')),
]
