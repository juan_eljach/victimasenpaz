const merge = require('webpack-merge');
const parts = require('./webpack/parts.js');
// Verificamos el lifecycle_event de npm, si es dev cargará devServer en la
// configuración de webpack, y correrá webpack-dev-server en vez de
// webpack-dev-middleware con nodeJS.
const dev = process.env.npm_lifecycle_event === 'dev' ? parts.devServer : null;
module.exports = merge(
  parts.common,
  dev,
  parts.loaderJS,
  parts.loaderJSON,
  parts.loaderCSS,
  parts.hotPlugin
);
