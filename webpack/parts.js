const webpack = require('webpack')
const path = require('path')
const PATHS = require('./paths')
const root = path.resolve('./')

exports.common = {
  resolve: {
    root: path.resolve('src'),
    modulesDirectories: ['node_modules'],
    alias: {
      scss: 'client/scss/app'
    },
    extensions: ['', '.js', '.jsx', '.scss']
  },
  entry: [
    'webpack-hot-middleware/client',
    path.resolve(root, PATHS.entry)
  ],
  output: {
    path: path.resolve(root, PATHS.dist),
    filename: 'app.js',
    publicPath: '/dist/'
  }
}
exports.devServer = {
  devServer: {
    contentBase: path.resolve('./dist'),
    historyApiFallback: true,
    hot: true,
    inline: true,
    noInfo: true
  }
}
exports.loaderJS = {
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        include: path.resolve(root, PATHS.js),
        query: {
          cacheDirectory: true,
          presets: ['react-hmre']
        }
      }
    ]
  }
}
exports.loaderJSON = {
  module: {
    loaders: [
      {
        test: /\.json?$/,
        loader: 'json-loader',
        include: path.resolve(root, PATHS.json),
      }
    ]
  }
}
exports.loaderCSS = {
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass'],
        include: path.resolve(root, PATHS.scss)
      }
    ]
  }
}
exports.hotPlugin = {
  plugins: [
    new webpack.HotModuleReplacementPlugin({
      multiStep: true
    })
  ]
}
