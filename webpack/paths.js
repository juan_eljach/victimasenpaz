module.exports = {
  entry: 'src/client/js/app.jsx',
  js: 'src/client/js',
  json: 'src/client/json',
  scss: 'src/client/scss',
  dist: 'dist'
};
